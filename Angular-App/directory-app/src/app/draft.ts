import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import  {DataService} from '../app/data.service'



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

//implement ngOninit to use inint function "event hook"
export class AppComponent implements OnInit {
  title = 'directory-app';

  directory!: Observable<any>;
  

  constructor(private data: DataService) { }

  ngOnInit() {
    this.directory = this.data.getdirectory()
  };
 
 

}

//html

<div *ngIf="(directory | async) as d">
    <div>{{ d.tree | json}}</div>
</div>

//
import {NestedTreeControl} from '@angular/cdk/tree';
import {Component} from '@angular/core';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import  {DataService} from '../app/data.service'


/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'Fruit',
    children: [
      {name: 'Apple'},
      {name: 'Banana'},
      {name: 'Fruit loops'},
    ]
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          {name: 'Broccoli'},
          {name: 'Brussels sprouts'},
        ]
      }, {
        name: 'Orange',
        children: [
          {name: 'Pumpkins'},
          {name: 'Carrots'},
        ]
      },
    ]
  },
];

/**
 * @title Tree with nested nodes
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  treeControl = new NestedTreeControl<FoodNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();

  


  constructor(private data: DataService) {  

  }

  ngOnInit() {
    this.dataSource.data = TREE_DATA;
    console.log(TREE_DATA)  };

  hasChild = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;
}

//html
   
   
   
<mat-tree [dataSource]="dataSource" [treeControl]="treeControl" class="example-tree">
<!-- This is the tree node template for leaf nodes -->
<!-- There is inline padding applied to this node using styles.
  This padding value depends on the mat-icon-button width. -->
<mat-tree-node *matTreeNodeDef="let node" matTreeNodeToggle>
    {{node.name}}
</mat-tree-node>
<!-- This is the tree node template for expandable nodes -->
<mat-nested-tree-node *matTreeNodeDef="let node; when: hasChild">
    <div class="mat-tree-node">
      <button mat-icon-button matTreeNodeToggle
              [attr.aria-label]="'Toggle ' + node.name">
        <mat-icon class="mat-icon-rtl-mirror">
          {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}
        </mat-icon>
      </button>
      {{node.name}}
    </div>
    <!-- There is inline padding applied to this div using styles.
        This padding value depends on the mat-icon-button width.  -->
    <div [class.example-tree-invisible]="!treeControl.isExpanded(node)"
        role="group">
      <ng-container matTreeNodeOutlet></ng-container>
  </div>
</mat-nested-tree-node>

s