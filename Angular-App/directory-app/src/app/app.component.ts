import {NestedTreeControl} from '@angular/cdk/tree';
import { ThrowStmt } from '@angular/compiler';
import {Component, OnInit} from '@angular/core';
import {MatTreeNestedDataSource} from '@angular/material/tree'; 
import { Observable } from 'rxjs';
import  {DataService} from '../app/data.service'

/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface FoodNode {
  name: string;
  children?: FoodNode[];
}

/*export interface TreeNode {
  id: string;
  name: string;
  level: number;
  expandable: boolean;
}*/


const TREE_DATA: FoodNode[] = [
  {
    name: 'Fruit',
    children: [
      {name: 'Apple'},
      {name: 'Banana'},
      {name: 'Fruit loops'},
    ]
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          {name: 'Broccoli'},
          {name: 'Brussels sprouts'},
        ]
      }, {
        name: 'Orange',
        children: [
          {name: 'Pumpkins'},
          {name: 'Carrots'},
        ]
      },
    ]
  },
];

/**
 * @title Tree with nested nodes
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  treeControl = new NestedTreeControl<FoodNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();

  directory!: Observable<any>;


  constructor(private data: DataService) {  }
  
  ngOnInit() {
    this.directory = this.data.getdirectory();
    this.directory.subscribe(((response) => {
      console.log(response);
      console.log(TREE_DATA);
      this.dataSource.data = response.tree.children;
    }));

    console.log(TREE_DATA);
    console.log(this.directory);
  };

  hasChild = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;
}