const express = require('express') 
const app = express();

const cors = require('cors');
app.use(cors({
  origin: '*'
}));

const dirTree = require("directory-tree");
const filteredTree = dirTree('Angular-App/directory-app/src', {attributes:['mode', 'ctime']});


const tree = dirTree('Angular-App/directory-app/src');
console.log(tree)

app.use((req, res, next) => {
  res.status(200).json({
    tree
  })
})

module.exports = app;